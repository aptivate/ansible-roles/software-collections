import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_scl_is_installed(host):
    package = host.package('centos-release-scl')
    assert package.is_installed


def test_sclo_rh_repository_is_enabled(host):
    cmd = host.run('yum repolist enabled | grep centos-sclo-rh | wc -l')

    assert cmd.rc == 0
    assert int(cmd.stdout.strip()) == 1


def test_sclo_sclo_repository_is_enabled(host):
    cmd = host.run('yum repolist enabled | grep centos-sclo-sclo | wc -l')

    assert cmd.rc == 0
    assert int(cmd.stdout.strip()) == 1
