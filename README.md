[![pipeline status](https://git.coop/aptivate/ansible-roles/software-collections/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/software-collections/commits/master)

# software-collections

Sets up and enables Software collection repository and functionality in CentOS 6.

To catch potential bugs/misunderstandings this will fail with an error if run on a different distribution/version. To avoid an error test for distribution/version when including the role (see the Example Playbook below)

We can add support for other distributions/versions when needed.

# Requirements

None

# Role Variables

This role uses `ansible_distribution` and `ansible_distribution_major_version` to determine the current OS version.

# Dependencies

None

# Example Playbook

```yaml
- hosts: localhost
  gather_facts: true
  roles:
     - role: software-collections
       when:
          - ansible_distribution == "CentOS"
          - ansible_distribution_major_version == "6"
```

# Testing

To test this, ensure you've installed the dev dependencies of the role:

```
$pipenv install --dev
```

To run the tests do:

```
$pipenv run molecule test
```

Or the vagrant variant:

```
$pipenv run molecule test -s vagrant
```

The tests should pass. As this is a CentOs 6 and 7 module at the moment, the default scenario runs on CentOS 6 and 7. To make sure the role fails with an error on other platforms, you can test the 'fail' scenario:

```
$pipenv run molecule test -s fail
```

The expected outcome is that Ansible will fail to turn.

# License

  * https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

  * https://aptivate.org/
  * https://git.coop/aptivate
